export default {
  data() {
    return {
      isLightTheme: false,
    };
  },
  mounted() {
    this.checkThemeState();
  },
  methods: {
    toggleTheme() {
      this.isLightTheme = !this.isLightTheme;
      localStorage.setItem("themeMode", JSON.stringify(this.isLightTheme));
    },
    checkThemeState() {
      this.isLightTheme = JSON.parse(localStorage.getItem("themeMode")) ?? false;
    },
  },
};
