import Vue from "vue";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import "./assets/default.scss";

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;

new Vue({
  router,
  render(h) { return h(App); },
}).$mount("#app");
