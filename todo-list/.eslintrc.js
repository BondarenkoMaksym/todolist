module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
    "@vue/airbnb",
  ],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    "import/no-unresolved": "off",
    "no-restricted-syntax": "off",
    "vue/singleline-html-element-content-newline": "off",
    quotes: "off",
    "linebreak-style": 0,
    "no-undef": "off",
    "no-shadow": "off",
    "import/extensions": "off",
    "no-param-reassign": ["error", {
      props: true,
      ignorePropertyModificationsFor: [
        "state",
        "app",
        "context",
        "event",
        "e",
        "config",
        "$axios",
        "$i18n",
        "jsonApiData",
      ],
    }],
    "import/no-extraneous-dependencies": "off",
    "no-underscore-dangle": "off",
    "vue/no-v-html": "off",
    "max-len": ["error", { code: 120, ignoreStrings: true }],
    "no-plusplus": "off",
    "vue/prop-name-casing": ["error", "camelCase"],
    "vue/attribute-hyphenation": ["error", "never", {
      ignore: [],
    }],
    "vue/attributes-order": "off",
    "vue/order-in-components": ["error", {
      order: [
        "name",
        "mixins",
        "components",
        "data",
        ["props", "propsData"],
        "LIFECYCLE_HOOKS",
        "computed",
        "methods",
      ],
    }],
  },
};
